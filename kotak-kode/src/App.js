// import logo from './logo.svg';
import React from 'react';
import Navbar from "./components/NavBar/Navbar"
import Main from "./components/Main/Main"
import Footer from "./components/Footer/Footer"
import './App.css';
import { div } from 'prelude-ls';


// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

// export default App;

function App() {
  return (
    <div className="App">
    <Navbar /> 
    <Main /> 
    <Footer />
    
    </div>
  );    
}

export default App;
