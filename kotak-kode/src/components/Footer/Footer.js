import React, { Component }  from 'react'

const Footer=()=>{
return(
<div style={{ display : 'flex', height:150, backgroundColor:'#1c1d1f', color:'white', justifyContent:'center', alignItems:'center',flexDirection:'column'}}>
<span>PT. Kota Digital Nusantara</span>
<span>Copyright 2020 Kotakode. All right reserved</span>
<span>Made with Love in ID</span>

</div>
)
}

export default Footer