import React, { Component }  from 'react'
import logo from '../NavBar/logo.png';
// import styled from 'styled-components'

const styles={
    input:{
        width: '100%',
        height: 32,
        fontSize: 14,
        marginTop: 8,
        paddingLeft: 10,
        borderRadius: 11,
        marginBottom: 5,
        color:'#000',
        alignItems:'center'
    },
    btn:{
        width:'100%',
        padding: 10,
        fontSize: 14,
        marginTop:20,
        outline: 'none',
        borderWidth: 2,
        borderRadius: 5,
        fontWeight: 700,
        cursor: 'pointer',
        alignItems: 'center',
        justifyContent: 'center',
        color: 'rgb(255, 255, 255)',
        background: 'rgb(249, 129, 58)',
        borderColor:'rgb(249, 129, 58)',
    }
}

// const FormContainer=styled.div`
//     width:450px;
//     padding:30px;
// `

// const Text=styled.span`
//     color: rgb(66, 90, 112);
//     font-size: 14px;
//     font-weight: bold;
//     line-height: 17px;
//`

const Input =(props)=>{
    return(
        <div>
            <span>{props.title}</span>
            <input style={styles.input} />
        </div>
    )
}

const Button =(props)=>{
    return(
        <button style={styles.btn}>{props.title}</button>
    )
}
class Main extends Component {
    render() {
        return(
            <div style={{ display: 'flex', justifyContent: 'center', flexDirection:'column', alignItems:'center', height:'80vh' }}>
                <img src={logo} style={{width:'180px', height:'55px'}}/>
                <div style={{width:450}}>
                    <Input title='Email/Username'/>
                    <Input title='Password'/>
                    <p style={{float:'right', marginTop:'5px' ,color:'#F9813A'}}>Lupa Password?</p>
                    <Button title='Masuk'/>
                </div>
            </div>
        )
    }
}

export default Main